package es.udc.asi.postexamplerest.model.service.dto;

import javax.validation.constraints.NotEmpty;

import es.udc.asi.postexamplerest.model.domain.Tag;

public class TagDTO {
  private Long id;
  @NotEmpty
  private String name;
  private String color;

  public TagDTO() {
  }

  public TagDTO(Tag tag) {
    this.id = tag.getId();
    this.name = tag.getName();
    this.color = tag.getColor();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }
}
