package es.udc.asi.postexamplerest.model.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.asi.postexamplerest.model.domain.Post;
import es.udc.asi.postexamplerest.model.domain.Tag;
import es.udc.asi.postexamplerest.model.exception.NotFoundException;
import es.udc.asi.postexamplerest.model.repository.PostDao;
import es.udc.asi.postexamplerest.model.repository.TagDao;
import es.udc.asi.postexamplerest.model.service.dto.TagDTO;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class TagService {

  @Autowired
  private TagDao tagDAO;

  @Autowired
  private PostDao postDAO;

  public List<TagDTO> findAll() {
    return tagDAO.findAll().stream().sorted(Comparator.comparing(Tag::getName)).map(TagDTO::new)
        .collect(Collectors.toList());
  }

  public TagDTO findById(Long id) throws NotFoundException {
    Tag tag = tagDAO.findById(id);
    if (tag == null) {
      throw new NotFoundException(id.toString(), Post.class);
    }
    return new TagDTO(tag);
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @Transactional(readOnly = false)
  public void deleteById(Long id) throws NotFoundException {
    List<Post> posts = postDAO.findAllByTag(id);
    Tag theTag = tagDAO.findById(id);
    if (theTag == null) {
      throw new NotFoundException(id.toString(), Tag.class);
    }
    posts.forEach(post -> {
      post.getTags().remove(theTag);
      postDAO.update(post);
    });
    tagDAO.delete(theTag);
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @Transactional(readOnly = false)
  public TagDTO create(TagDTO tag) {
    Tag bdTag = new Tag(tag.getName(), tag.getColor());
    tagDAO.create(bdTag);
    return new TagDTO(bdTag);
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @Transactional(readOnly = false)
  public TagDTO update(TagDTO tag) throws NotFoundException {
    Tag bdTag = tagDAO.findById(tag.getId());
    if (bdTag == null) {
      throw new NotFoundException(tag.getId().toString(), Tag.class);
    }
    bdTag.setName(tag.getName());
    bdTag.setColor(tag.getColor());
    tagDAO.update(bdTag);
    return new TagDTO(bdTag);
  }
}
