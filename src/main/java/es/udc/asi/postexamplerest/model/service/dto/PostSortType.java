package es.udc.asi.postexamplerest.model.service.dto;

public enum PostSortType {
  MOST_RECENT, LESS_RECENT, AUTHOR_NAME
}
