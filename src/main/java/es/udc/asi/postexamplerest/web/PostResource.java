package es.udc.asi.postexamplerest.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.udc.asi.postexamplerest.model.domain.Post;
import es.udc.asi.postexamplerest.model.exception.NotFoundException;
import es.udc.asi.postexamplerest.model.exception.OperationNotAllowed;
import es.udc.asi.postexamplerest.model.service.PostService;
import es.udc.asi.postexamplerest.model.service.dto.PostDTO;
import es.udc.asi.postexamplerest.model.service.dto.PostSortType;
import es.udc.asi.postexamplerest.web.exceptions.IdAndBodyNotMatchingOnUpdateException;
import es.udc.asi.postexamplerest.web.exceptions.RequestBodyNotValidException;

@RestController
@RequestMapping("/api/posts")
public class PostResource {

  @Autowired
  private PostService postService;

  @GetMapping
  public List<PostDTO> findAll(@RequestParam(required = false) String query,
      @RequestParam(required = false) PostSortType sort,
      @RequestParam(required = false) String tag) {
    return postService.findAll(query, tag, sort);
  }

  @GetMapping("/{id}")
  public PostDTO findOne(@PathVariable Long id) throws NotFoundException {
    return postService.findById(id);
  }

  @PostMapping
  public PostDTO create(@RequestBody @Valid PostDTO post, Errors errors) throws RequestBodyNotValidException {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }

    return postService.create(post);
  }

  @PutMapping("/{id}")
  public PostDTO update(@PathVariable Long id, @RequestBody @Valid PostDTO post, Errors errors)
      throws IdAndBodyNotMatchingOnUpdateException, RequestBodyNotValidException, NotFoundException,
      OperationNotAllowed {
    if (errors.hasErrors()) {
      throw new RequestBodyNotValidException(errors);
    }

    if (id != post.getId()) {
      throw new IdAndBodyNotMatchingOnUpdateException(Post.class);
    }
    return postService.update(post);
  }

  @DeleteMapping("/{id}")
  public void delete(@PathVariable Long id) throws NotFoundException, OperationNotAllowed {
    postService.deleteById(id);
  }

}
